# purple-matrix-puppet

This is a python3 script used to bridge Matrix and SMS/MMS messages via purple with [purple-mm-sms](https://source.puri.sm/Librem5/purple-mm-sms). 

This script is a complete copy of ofono-matrix-puppet. All calls to ofono have been replaced with purple. Basic contact handling has also been added.

It was made specificly for purple-mm-sms and chatty combo. It won't interfere with them either, so you can still text from chatty with or without this script running. With some changes, it could probably be adapted for other clients/protocols. 

It is experimental - there may be issues here and there. Make sure to use it carefully. I am not responsible for anything that happens as a result of using this script.

## Status
**Cell -> Matrix**
* [x] SMS
* [ ] MMS
* [ ] Group Messages


**Matrix -> Cell**
* [x] m.text - SMS for normal chat. MMS for group chats.
* [ ] m.image - Sent as MMS. If its too large send as a URL.
* [x] m.XXXXX - Everything else is sent as a URL.


**General**
* [x] Initiate a new conversation in Matrix
* [x] Leave room when user leaves and clean up database.
* [ ] Clean up bridged MMS files.
* [x] Retry failed messages to Matrix
* [x] Retry failed messages to purple
* [x] Contacts handling
* [ ] Ignore list


## TODO:

* Gifs dont move until you click on them.
* https://github.com/matrix-org/matrix-python-sdk/pull/303/commits/aa4071835a79b5a3dccfd366b01bd9d53ed0a62f
* Clean up loggin
* testing/fixing bugs
* any TODOs I left in the file and didn't clean up.
* Replace ofono with purple throughout file, mainly for semantics





# Requirements

- Modem compatible with modemmanager 
- python >= 3.5.0
- python3-pip
- pip3 install phonenumbers
- pip3 install matrix_client *

***NOTE:** If you want to have a matrix sync timeout in this script that's greater 
 than a couple second, you'll want to manually apply [this](https://github.com/matrix-org/matrix-python-sdk/pull/303/commits/aa4071835a79b5a3dccfd366b01bd9d53ed0a62f)
 patch in the matrix_client.

I use 60 seconds in the matrix-python-sdk and 55 seconds (55000 ms) in my script.

## Install  

0.  Setup your own homeserver.. or use matrix.org via riot - https://riot.im/app/#/register
1.  If you don't have an account, Create one.
2.  Create an account for the bot.
3.  Grab the latest version of this script.
4.  Update the settings at the top to have your information.
5.  read the rest of the settings and set them.
6.  run and test the app
7.  submit issues or merge requests to fix any problems.




## Usage:

All you need is python3, a Linux box, and a modem that works with modemmanager 

Download the python script and edit the variables at the top. The password is
 only required on the first run. If the login was successful your token will be 
 stored in the database.

To see if the script is working just tail the logs and watch it for a few seconds. 
 you should see that it logged in, got a token, and is syncing every X seconds.
 Nothing should print to the terminal, everything should be in the logs.
 Once the script is running everything should now be bridged into matrix.
 
I've tried to keep all personal information (Phone numbers, message content) in
 the debug (10) log messages. Info (20) should tell you what it's doing without
 logging SMS content or phone numbers.
 

**Starting a Conversation from Matrix**

*  **Room Name** - Name of contact.
*  **Room Topic** - Phone number of contact. If a group a csv list of numbers.

Create a room, Set the room name to the name of the person you wish to contact,
 and the topic to their phone number. Once you have a room with this information
 invite the bot account. It will Join the room and check if the topic is a
 phone number. If this is a new group chat separate each phone number with a
 comma.
 
If the bot finds a problem it will tell you about it and leave the room. Fix the
 problem and reinvite the bot.
 
Once you have a good room the bot will change their display name to the room
 name and update the database matching the room ID (!TdGYixONCkUyUOsUnf:matrix.org)
 to the cellphone number. Any messages posted to this room will then be send to
 the cellphone via purple. Any reply from them will be posted in the same room.

If you change the room name, the bot will update their display name. If you
 change the topic nothing will happen. The phone number is matched in the
 database with the roomID so you probably want to make a new room.
 
**Receiving a message for a new room**

When you receive a new message the bot will check the local database for a room.
 If there is no room the bot will create a room, update the Room Name and Topic
 to the phone number, change their display name to the phone number, update the 
 power levels so you cant change the topic and invite the user (you). If you
 change the room name the bot will update their display name.
 
If there is a room already, the bot will simply post the message to that room.

Any replies posted to the room the bot will send to the cellphone number.


**Pictures/MMS/Files/ETC**

Will not work until purple-mm-sms/modemmanager stack has mms support. Code will be commented out.

MMSes are limited in the size you can send. From my experience Android will 
 compress pictures to about 300K but I was able to get away with ~650K. This 
 will depend on your provider so you may need to test.
 
If you upload something matrix considers an "m.image" the bot will check that 
 the size is less than what you've set. If its less than our limit it will 
 download the file and send it as an MMS. If the m.image file is larger than our
 limit it will just send an SMS with the matrix URL for the user to click.
 
Any other files uploaded to matrix, regardless of size are send an SMS link. 


Group messages are always sent as MMSes.


**Leaving a room**

When you leave a room the bot will delete everything related to that room then 
 it will also leave the room it self. This means it will create a new room if 
 it needs to.
 
 
 **General**
 
You can view the *source of a message* in riot. This will give you information 
  like the ID in the sqlite3 database, what attepmt it was on to bridge the
  message, and what time it was sent.

The *read receipts* will move after the messages has been written to the sqlite3
 database. This lets you know that the bot is working and at least the message 
 made it to the database. This may change when I figure out how ofono lets us
 know the message was successfuly sent.
 
The *online presence* of the bot should work as expected.

The *topic* is mostly used for the phone number you're in contact with. If you
 change the topic it will do nothing. The room is matched by the room ID and 
 the phone number listed in the database. If you make a mistake its best to
 leave the room and make a new one.
 
 
Ubuntu Touch script that'll start the program on boot. If your phone is off and
 someone sends you a message, This will not always start the bridge fast enough
 to catch them when you boot the phone.

phablet@ubuntu-phablet:~$ cat /usr/share/upstart/sessions/z-matrix-ofono.conf

    
    description "das boot"
    start on started unity8-dash

    exec /home/phablet/matrix-ofono-puppet/matrix-ofono-puppet.py
