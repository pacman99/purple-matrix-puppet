#!/usr/bin/env python3

###########################
#####Do not edit below#####
###########################
#Required for logging
import logging

#Threading to listen on dbus twice and web once
import threading

#For login and to get token
from matrix_client.client import MatrixClient
#Main uses MatrixHttpApi
from matrix_client.api import MatrixHttpApi

#For matrix_sync_every_seconds sleep
import time

import datetime

#for dbus listening - sms/mms recieve.
from gi.repository import GLib
import dbus
import dbus.mainloop.glib
import sys

#random numbers just in case we get 2 texts at the same time
import random

#used to create mms path if it doesn't exist.
from pathlib import Path
from difflib import SequenceMatcher

#Database to retry failed messages.
import sqlite3

#for building outbound MMSes
import csv

#to download matrix file
import urllib.request

#for configuration
import os.path
import configparser

#Deal with phone numbers
import phonenumbers

#to parse contacts file
import re

#delete attachments when user leaves room
import shutil

#enums
from enum import Enum
class Ofono_types(Enum):
  SMS   = 1
  MMS   = 2

class Ofono_direction(Enum):
  IN  = 1 #ofono to matrix
  OUT = 2 #matrix to ofono

class Message_status(Enum):
  PENDING = 1
  TRYING  = 2
  BRIDGED = 3
  ERROR   = 4

#Handle configuration
home_directory = os.path.expanduser('~')
possible_config_files = ['config.ini',
  home_directory+'/.config/purple-matrix-puppet/config.ini']

config = configparser.ConfigParser()

config['MATRIX'] = {'ServerUrl': 'https://matrix-client.org'}
config['PHONE'] = {'DefaultCountry': 'CA'}
config['DATA'] = {
  'MMSSizeLimit': '650000',
  'DataDirectory': '/home/alarm/.local/share/purple-matrix-puppet',
  'LogLevel': '10'
}

for config_file in possible_config_files:
  if os.path.exists(config_file):
    config.read(config_file)

matrix_url = config['MATRIX']['ServerUrl']

bot_user   = config['MATRIX']['BotUser']
bot_pass   = config['MATRIX']['BotPassword']

matrix_user = config['MATRIX']['MatrixUser']

#Number information
#https://github.com/daviddrysdale/python-phonenumbers/tree/dev/python
default_country = config['PHONE']['DefaultCountry']
my_cell_number = config['PHONE']['CellNumber']

#MMS file size limit. Anything larger will send a URL as an SMS.
#You might have to play with this. below is the ~largest file I could send.
mms_size_limit = config['DATA']['MMSSizeLimit']

#everything we do will be put under here.
script_home = config['DATA']['DataDirectory']

contacts_file = config['DATA']['ContactsFile']

#Wait X ms for a new event from matrix before building a new connection.
matrix_sync_timeout_ms = 55000

#pause for X seconds after closing the connection before building a new one.
matrix_sync_every_seconds = 2

#Retry messages that have failed to be bridged.
#set to 0 to disable.
check_failed_messages_every_seconds = 60
max_number_of_retries = 30

#Log Settings
log_level = int(config['DATA']['LogLevel'])
log_size_bytes = 10000000
log_backup_count = 2


database_path = script_home+"/database/"
database_file = database_path+"/main.sqlite3"
Path(database_path).mkdir(parents=True, exist_ok=True)

attachments_path = script_home+"/attachments/"
Path(attachments_path).mkdir(parents=True, exist_ok=True)

#dbus paths
purpleDbus = "im.pidgin.purple.PurpleService"
purpleObj = "/im/pidgin/purple/PurpleObject"


##Setup Logging##

lock_log = threading.Lock()

logger = logging.getLogger('matrix-ofono-puppet')
logger.setLevel(logging.DEBUG)

# Here we define our formatter
formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')

log_handler = logging.StreamHandler(sys.stdout)
log_handler.setLevel(log_level)
log_handler.setFormatter(formatter)

logger.addHandler(log_handler)


#supporting functions
def format_number(number):
  with lock_log:
    logger.debug("[format_number] - called.")
    logger.debug("[format_number] - pre: %s",number)

  to_format=phonenumbers.parse(number, default_country)
  formatted_number=phonenumbers.format_number(to_format, phonenumbers.PhoneNumberFormat.E164)

  with lock_log:
    logger.debug("[format_number] - post: %s",formatted_number)

  return formatted_number

def test_number(number):
  with lock_log:
    logger.debug("[test_number] - called.")
  try:
    formatted=phonenumbers.parse(number, default_country) 
    tested_number=phonenumbers.is_possible_number(formatted)
  except Exception as problem:
    logger.error('[matrix_deal_with_invites] - I dont think this is a phone number: %s', problem)
    return False

  return tested_number

#format the users input just in case.
formatted_cell_number = format_number(my_cell_number)


def build_database(database_connection):
  with lock_log:
    logger.debug("[build_database] - called.")

  # Get a cursor object
  cursor = database_connection.cursor()
  cursor.execute('''CREATE TABLE IF NOT EXISTS messages (
                    id INTEGER PRIMARY KEY, 
                    rooms_id INTEGER,
                    sender TEXT,
                    direction INTEGER,
                    time TEXT,
                    ofono_type INTEGER,
                    content_type TEXT,
                    content TEXT,
                    file_size INTEGER,
                    bridged_status INTEGER,
                    bridged_attempt INTEGER,
                    event TEXT )''')
  database_connection.commit()

  cursor.execute('''CREATE TABLE IF NOT EXISTS rooms (
                    id INTEGER PRIMARY KEY, 
                    room TEXT,
                    member TEXT )''')
  database_connection.commit()
 
  cursor.execute('''CREATE TABLE IF NOT EXISTS group_members (
                    id INTEGER PRIMARY KEY, 
                    mmsgroup TEXT,
                    number TEXT )''')
  database_connection.commit()

  cursor.execute('''CREATE TABLE IF NOT EXISTS matrix_account (
                    id INTEGER PRIMARY KEY, 
                    token TEXT,
                    sync TEXT )''')
  database_connection.commit()

def database_check_matrix_account(database_connection):
  with lock_log:
    logger.debug("[database_check_matrix_account] - called.")


  cursor = database_connection.cursor()
  cursor.execute('''SELECT token, sync FROM matrix_account WHERE id=1''')
  row = cursor.fetchone() #retrieve the first row
  if ( row != None):
    with lock_log:
      logger.debug("[database_check_matrix_account] - token %s",row[0])
    with lock_log:
      logger.debug("[database_check_matrix_account] - sync %s",row[1])
    return row


  else:
    with lock_log:
      logger.info("[database_check_matrix_account] - no information")
    cursor.execute('''INSERT INTO matrix_account(token, sync)
                   VALUES("","")''')
    database_connection.commit()
    return None

def database_matrix_update_token(database_connection, token):
  with lock_log:
    logger.debug("[database_matrix_update_token] - called.")
  cursor = database_connection.cursor()
  cursor.execute('''update matrix_account set token=? where id=1''',  (token,) )
  database_connection.commit()
  return 1
 
def database_matrix_update_sync(database_connection, sync):
  with lock_log:
    logger.debug("[database_matrix_update_sync] - called.")
  cursor = database_connection.cursor()
  cursor.execute('''update matrix_account set sync=? where id=1''',  (sync,) )
  database_connection.commit()
  return 1

def matrix_join_room(matrix_thread_api, room):
  with lock_log:
    logger.debug("[matrix_join_room] - called")
    logger.debug('[matrix_join_rooms] - trying to join: %s',room)
  path=str("/join/"+room)
  method="POST"
  try: matrix_thread_api._send(method=method, path=path)
  except Exception as problem:
    with lock_log:
      logger.error('[matrix_join_room]-Error %s',problem)
    return 1

  return 0

def matrix_get_topic(matrix_thread_api, room_id):
  with lock_log:
    logger.debug("[matrix_get_topic] - called.")
  dict_topic = {}
  try: dict_topic=matrix_thread_api._send("GET", "/rooms/"+room_id+"/state/m.room.topic")
  except Exception as problem:
    with lock_log:
      logger.error("[matrix_get_room_name] - problem %s", problem)
    dict_topic['topic'] = "failed"
  try: topic = dict_topic['topic']
  except: topic = "failed"
  return topic

def database_get_room(database_connection, room_members):
  #in a 1:1 SMS, there are 2 room members.
  #in a 1:X MMS there will be >2 room_members. (include yourself)
  with lock_log:
    logger.debug("[database_get_room] - called.")

  if(type(room_members) == list):
    #get a var without me in it.
    if(len(room_members) >= 2):
      other_people = room_members.copy()
      other_people.remove(formatted_cell_number)
      other_people.sort()


    #This is a 1:1 message.
    if(len(room_members) == 2):
      with lock_log:
        logger.debug("[database_get_room] - 1:1 chat sender %s", other_people)

      #other people should only have the sender in it as we removed ourself.
      cursor = database_connection.cursor()
      cursor.execute('''SELECT id FROM rooms WHERE member=?''', (other_people[0],))

      #get the first row and return it, else return 0
      row = cursor.fetchone()
      database_connection.commit()
      if ( row != None and row[0] > 0):
        with lock_log:
          logger.debug("[database_get_room] - found local room %s",row[0])
        return row[0]
      else:
        with lock_log:
          logger.info("[database_get_room] - No Local room.")
        return 0


    #This is a group message.
    elif(len(room_members) > 2):
      with lock_log:
        logger.debug("[database_get_room] - group chat room_members %s", room_members)


      #get all the groups one of the participants is in
      cursor = database_connection.cursor()
      cursor.execute('''SELECT mmsgroup FROM group_members WHERE number=?''', (other_people[0],))
      all_possible_groups = cursor.fetchall()
      database_connection.commit()

      #for all the groups that the participant is in.
      for group in all_possible_groups:

        #get all the members in each group
        cursor.execute('''SELECT number FROM group_members WHERE mmsgroup=?''', (group[0],))
        all_members_in_group_rows = cursor.fetchall()
        database_connection.commit()

        #build a list to compare with what we were provided.
        all_group_members = []
        for row in all_members_in_group_rows:
          all_group_members.append(row[0])

        #compare the two lists.
        all_group_members.sort()  
        if(all_group_members == other_people):

          #if we found a matching list of members, get the room ID
          cursor.execute('''SELECT id FROM rooms WHERE member=?''', (group[0],))
          room_id = cursor.fetchone()
          database_connection.commit()
          if ( room_id != None and room_id[0] > 0):
            with lock_log:
              logger.debug("[database_get_room] - found room %s", room_id[0])
            return room_id[0]
          else:
            with lock_log:
              logger.critical("[database_get_room] - DB out of sync. found a group in group_members but not rooms")
            return 0


  else:
    with lock_log:
      logger.critical("[database_get_room] - pass a list of all members.")
  return 0;

def database_build_room(database_connection, room_members):
  with lock_log:
    logger.debug("[database_build_room] - called.")

  if(type(room_members) == list):
    #get a var without me in it.
    if(len(room_members) >= 2):
      other_people = room_members.copy()
      other_people.remove(formatted_cell_number)


    #This is a 1:1 message.
    if(len(room_members) == 2):
      with lock_log:
        logger.debug("[database_build_room] - create room with %s", other_people[0])
      cursor = database_connection.cursor()
      cursor.execute('''INSERT INTO rooms(member)
                      VALUES(?)''',  (other_people[0],) )

      database_connection.commit()
      with lock_log:
        logger.info("[database_build_room] - built room with ID %s", cursor.lastrowid)
      return cursor.lastrowid


    #This is a group message.
    elif(len(room_members) > 2):
      with lock_log:
        logger.debug("[database_build_room] - build group chat %s", other_people)

      #Build a semi random group room name.
      group ="group:"+str(int(time.time()))+str(random.randrange(10, 99))

      cursor = database_connection.cursor()
      cursor.execute('''INSERT INTO rooms(member)
                     VALUES(?)''', (group,))
      database_connection.commit()
      id = cursor.lastrowid

      for number in other_people:
        cursor.execute('''INSERT INTO group_members (mmsgroup,number)
                       VALUES(?,?)''', (group,number,))
        database_connection.commit()
      return id

def matrix_set_user_as_admin(matrix_api, room_id):
  with lock_log:
    logger.debug("[matrix_set_user_as_admin] - called.")
  json_body = {"users":{
                       bot_user:100,
                       matrix_user:99
                       },
               "users_default":0,
               "events":{
                        "m.room.name":50,
                        "m.room.topic":100,
                        "m.room.power_levels":100,
                        "m.room.history_visibility":99,
                        "m.room.canonical_alias":50,
                        "m.room.avatar":50,
                        "m.room.aliases":0,
                        "m.room.tombstone":99,
                        "m.room.server_acl":99
                        },
               "events_default":0,
               "state_default":50,
               "ban":50,
               "kick":50,
               "redact":50,
               "invite":0
               }

  try:
    return_event = matrix_api._send("PUT", "/rooms/"+room_id+"/state/m.room.power_levels/", json_body)
  except Exception as problem:
    with lock_log:
      logger.error("[matrix_set_user_as_admin] - problem %s", problem)
    return_event = None

  return return_event

def deal_with_mms_attachment(database_connection, rooms_id, sender, attachment ):
  #this takes one attachment string and returns a list
  #return[0] is the type - image/jpeg - image/gif - application/pdf - etc
  #return[1] is either the plaintext message or the path to the file.
  #a path with start with '/' strings will not.

  with lock_log:
    logger.debug("[deal_with_mms_attachment] - called.")

  in_file = open(attachment[2], "rb")
  header  = in_file.read(attachment[3]) # the header is like the smil or something
  data    = in_file.read(attachment[4])
  in_file.close()
  header=None

  file_pack=[]
  file_pack.append( attachment[1] )

  if(attachment[1] == "text/plain;charset=utf-8"):
    with lock_log:
      logger.info("[deal_with_mms_attachment] - attachment is text.")

    #I check if 'message' starts with / then upload a file.
    #if a message starts with / add a space to the start. This way if someone sends /etc/passwd we dont upload that
    if (data[0] == "/"):
      data=" "+data
    file_pack.append( data.decode("utf-8") )

    return file_pack

  else: 
    with lock_log:
      logger.info("[deal_with_mms_attachment] - Attachment is a file.")


    #create a folder for the room the image is for.
    attachment_room_path = attachments_path+"/"+str(rooms_id)+"/"
    Path(attachment_room_path).mkdir(parents=True, exist_ok=True)


    #some android clients always use image.jpg so if they send two pictures it will have the same name.
    #so we get some extra random stuff to avoid duplicates
    outfile=attachment_room_path+str(int(time.time()))+"-"+str(random.randrange(10, 99))+sender+"_"+attachment[0][1:-1]

    #take whats after / in the attachment type and add it to the filename.
    #so image/gif makes file.gif
    #application/pdf makees file.pdf 
    #etc
    outfile += "."+attachment[1].split("/",1)[1]

    out_file = open(outfile, "wb")
    out_file.write(data)
    out_file.close()
    file_pack.append( outfile )

    return file_pack

  return None

def os_delete_attachments(rooms_id):
  with lock_log:
    logger.debug("[os_delete_attachments] - called.")

  if(rooms_id != None and rooms_id > 0):
    try:
      shutil.rmtree(attachments_path+str(rooms_id))
      with lock_log:
        logger.info("[os_delete_attachments] - deleted attachments: %s", attachments_path+str(rooms_id))
    except OSError as e:
      with lock_log:
        logger.error("[os_delete_attachments] - Room: %s : %s" % (rooms_id, e.strerror))

def database_write_message(database_connection, message):
  with lock_log:
    logger.debug("[database_write_message] - called.")
  cursor = database_connection.cursor()
  cursor.execute('''INSERT INTO messages(rooms_id, sender, direction, time, ofono_type, content_type, content, file_size, bridged_status, bridged_attempt)
                 VALUES(?,?,?,?,?,?,?,?,?,?)''', 
                 (message["rooms_id"], message["sender"], message["direction"], message["localsenttime"], message["ofono_type"], message["content_type"], message["content"], message["file_size"], Message_status.PENDING.value, 1))
  database_connection.commit()

  with lock_log:
    logger.info("[database_write_message] - New message written to DB. row id: %s", cursor.lastrowid )
  return cursor.lastrowid

def database_mark_bridged_status(database_connection, message_id, new_bridged_status, new_bridged_attempt, new_event):
  with lock_log:
    logger.debug("[database_mark_bridged_status] - called.")

  cursor = database_connection.cursor()
  cursor.execute('''UPDATE messages SET bridged_status = ?, bridged_attempt = ?, event = ? WHERE id = ? ''',
  (new_bridged_status, new_bridged_attempt, new_event, message_id ))
  database_connection.commit()

  with lock_log:
    logger.info("[database_mark_message_as] - message id %s updated - status %s - Attempt %s - event %s.", message_id, new_bridged_status, new_bridged_attempt, new_event)
  return 0

def database_ofono_sent_status(database_connection, event_path, new_bridged_status):
  with lock_log:
    logger.debug("[database_ofono_sent_status] - called.")

  cursor = database_connection.cursor()
  cursor.execute('''SELECT id FROM messages WHERE event=?''', (event_path,))
  row = cursor.fetchone() #retrieve the first row
  if ( row != None):
    message_id=row[0]
    cursor.execute('''UPDATE messages SET bridged_status = ? WHERE id = ? ''',
    (new_bridged_status, message_id ))
    database_connection.commit()

  else:
    with lock_log:
      logger.error("[database_ofono_sent_status] - ofono event %s not found in my database", event_path)
    return 1


  with lock_log:
    logger.info("[database_ofono_sent_status] - messages id %s  status %s .", message_id, new_bridged_status)
  return 0

def database_check_for_matrix_room(database_connection, rooms_id):
  with lock_log:
    logger.debug("[database_check_for_matrix_room] - called.")

  
  cursor = database_connection.cursor()
  cursor.execute('''select room from rooms WHERE id=?;''', (rooms_id,))
  row = cursor.fetchone() #retrieve the first row
  if ( row != None ):
    with lock_log:
      logger.info("[database_check_for_matrix_room] - Matrix Room: %s",row[0])
    return row[0]
  else:
    with lock_log:
      logger.info("[database_check_for_matrix_room] - No Matrix Room for local room.")
    return 0

def database_add_matrix_room(database_connection,rooms_id,room):
  with lock_log:
    logger.debug("[database_add_matrix_room] - called.")

  cursor = database_connection.cursor()

  cursor.execute('''UPDATE rooms SET room = ? WHERE id = ? ''',
  (room, rooms_id ))

  database_connection.commit()

  with lock_log:
    logger.debug("[database_add_matrix_room] - rooms_id %s is now %s", rooms_id, room)

def matrix_build_room(database_connection, message):
  global matrix_api

  with lock_log:
    logger.debug("[matrix_build_room] - called.")

  #try to build the matrix room. Else exit.
  try:
    room = matrix_api.create_room(is_public=False)
    matrix_room = room['room_id']
  except Exception as problem:
    with lock_log:
      logger.error("[matrix_build_room] - failed to build room %s", problem)
    return None

  #Build the new room name and topic vars
  if( len(message["recipients"]) > 1):
    name = message["recipients"].copy()
    name.append(message["sender"])
    name.remove(formatted_cell_number)


    topic = message["recipients"].copy()
    topic.append(message["sender"])
    topic.remove(formatted_cell_number)

  else:
    topic = message["sender"]
    name = message.get("sender_name")
    if not name:
      name = topic

  #set topic
  try: matrix_api.set_room_topic(matrix_room, str(topic))
  except Exception as problem:
    with lock_log:
      logger.error('[matrix_build_room] - failed to set topic to new room %s',problem)
    return None

  #set room name
  try: matrix_api.set_room_name(matrix_room, str(name))
  except Exception as problem:
    with lock_log:
      logger.error('[matrix_build_room] - failed to set name to new room %s',problem)
    return None

  with lock_log:
    logger.debug("[matrix_builld_room] - inviting %s to %s", matrix_user, matrix_room)
  #Set bot display name same as room name.
  try: matrix_set_display_name(matrix_api, matrix_room, str(name))
  except Exception as problem:
    with lock_log:
      logger.error('[matrix_build_room] - failed to set bot display name %s',problem)
    return None
    
  #invite user
  try: matrix_api.invite_user(matrix_room, matrix_user)
  except Exception as problem:
    with lock_log:
      logger.critical('[matrix_build_room] - failed to invite user to new room %s',problem)
    return None

  #try to give matrix_user admin
  matrix_set_user_as_admin(matrix_api, matrix_room )


  #if everything was done correctly, add it to the database for next time.
  database_add_matrix_room(database_connection, message["rooms_id"], matrix_room)

  # TODO search for contact of phone number and show user
  # In case purple contact handling fails
  #contact = find_contact(topic)
  #if contact:
  #    matrix_send_text_emote(matrix_api, matrix_room, f"Found contact for {topic}: {str(name)}")
  #
  with lock_log:
    logger.info("[matrix_build_room] - Built matrix room.")
    logger.debug("[matrix_build_room] - Built matrix room: %s ", matrix_room)
    

  return matrix_room

def database_matrix_room_to_rooms(database_connection, room_id):
  #returns list of room_id, member
  #member can be a number or a group:
  with lock_log:
    logger.debug("[database_matrix_room_to_rooms] - called.")

  rooms_info=[] 
  cursor = database_connection.cursor()
  cursor.execute('''select id, member from rooms where room=?;''', (room_id,))
  row = cursor.fetchone()
  database_connection.commit()
  if ( row != None ):
    with lock_log:
      logger.debug("[database_matrix_room_to_rooms] - Matrix room matches local room_id %s",row[0])
    rooms_info.append(row[0])
    rooms_info.append(row[1])
    return rooms_info

  else:
    with lock_log:
      logger.debug("[database_matrix_room_to_rooms] - Matrix room does not match any local rooms")
    return None

def database_get_list_of_group_members(database_connection, mmsgroup):
  with lock_log:
    logger.debug("[database_get_list_of_group_recipients] - called.")

  #get all the groups the sender is in.
  cursor = database_connection.cursor()
  cursor.execute('''SELECT number from group_members where mmsgroup=?''', (mmsgroup,))

  mms_group_numbers=[]

  all_numbers_in_mmsgroup = cursor.fetchall()
  for number in all_numbers_in_mmsgroup:
    if(number != formatted_cell_number):
      mms_group_numbers.append(number[0])

  database_connection.commit()
  if(len(mms_group_numbers) > 1):
    return mms_group_numbers
  else:
    return None

def matrix_read_receipt(matrix_thread_api, room_id, event_id):
  with lock_log:
    logger.debug("[matrix_read_receipt] - called")

  read_receipt = None
  try:
    read_receipt = matrix_thread_api._send("POST", "/rooms/"+room_id+"/receipt/m.read/"+event_id)
    with lock_log:
      logger.debug("[matrix_read_receipt] - room %s read up to %s",room_id,event_id)
  except Exception as problem:
    with lock_log:
      logger.error("[matrix_leave_room] - problem %s", problem)

  return read_receipt

def matrix_set_display_name(matrix_thread_api, room_id, display_name):
  #/_matrix/client/unstable/rooms/{roomId}/state/m.room.member/{stateKey}
  with lock_log:
    logger.debug("[matrix_set_display_name] - called.")

  json_body = { 'membership': 'join', 'displayname': display_name}
  try:
    matrix_thread_api._send("PUT", "/rooms/"+room_id+"/state/m.room.member/"+bot_user, json_body)

  except Exception as problem:
    with lock_log:
      logger.error("[matrix_set_display_name] - problem %s", problem)
  return 0

def matrix_leave_room(matrix_thread_api, room_id):
  with lock_log:
    logger.debug("[matrix_leave_room] - called.")
    
  try:
    matrix_thread_api._send("POST", "/rooms/"+room_id+"/leave")
  except Exception as problem:
    with lock_log:
      logger.error("[matrix_leave_room] - problem %s", problem)

  return 0

def database_delete_everything_for_room(database_connection, room_matrix_id):
  with lock_log:
    logger.debug("[database_delete_everything_for_room] - called.")

  cursor = database_connection.cursor()
  cursor.execute('''select id, member from rooms where room=?;''', (room_matrix_id,))
  row = cursor.fetchone() 
  if ( row != None ):
    with lock_log:
      logger.info("[database_delete_everything_for_room] - deleting everything for room %s",room_matrix_id)
      logger.debug("[database_delete_everything_for_room] - room table id %s",row[0])
      logger.debug("[database_delete_everything_for_room] - room table member %s",row[1])
    cursor.execute('''DELETE FROM rooms WHERE room=? ''', (room_matrix_id,))
    cursor.execute('''DELETE FROM messages WHERE rooms_id=? ''', (row[0],))
    cursor.execute('''DELETE FROM group_members WHERE mmsgroup=? ''', (row[1],))
    database_connection.commit()
    return row[0]

  else:
    with lock_log:
      logger.debug("[database_delete_everything_for_room] - Matrix room does not match any local rooms")
    return None
 
def matrix_send_text_emote(matrix_thread_api, room_id, notice_message):
  with lock_log:
    logger.debug("[matrix_send_text_emote] - called.")

  content_pack = {
    "msgtype": "m.emote",
    "body": notice_message,
  }

  try:
    matrix_api.send_message_event(room_id=room_id,event_type="m.room.message",content=content_pack)
  except Exception as problem:
    with lock_log:
      logger.critical("[matrix_send_text_emote] - error: %s", problem)
    return None

  return 0

def get_purple_account():
  with lock_log:
    logger.debug("[get_purple_account] - Called")

  bus = dbus.SessionBus()
  manager = dbus.Interface(bus.get_object(purpleDbus, purpleObj),
                                  purpleDbus+'.PurpleAccount')
  accounts = manager.PurpleAccountsGetAllActive()
  checkAccount = lambda a: manager.PurpleAccountGetProtocolName(a) == 'ModemManager SMS'
  possibleAccounts = filter(checkAccount, accounts)
  try:
    account = next(possibleAccounts)
  except StopIteration as problem:
    with lock_log:
      logger.critical("[get_purple_account] - error finding purple account for purple-mm-sms")
    return None

  return account
   
def checkName(query, name):
  query = query.split()
  name = name.split()
  for q in query:
    for n in name:
      matcher = SequenceMatcher(None, n.lower(), q.lower())
      if matcher.ratio() > .85:
        return True
  return False

def find_contact(name):
  name = name
  account = get_purple_account()
  if not account:
    return None

  #buddy manager
  bus = dbus.SessionBus()
  bm = dbus.Interface(bus.get_object(purpleDbus, purpleObj),
                                    purpleDbus+'.PurpleBuddy')

  for buddy in bm.PurpleFindBuddies(account, ''):
    cname = bm.PurpleBuddyGetAlias(buddy)
    if checkName(name, cname):
      return (str(cname), str(bm.PurpleBuddyGetName(buddy)))

  # try fallback with contacts file
  if 'contacts_file' in globals():
    contacts = open(contacts_file, 'r')
    line = contacts.readline()
    found = False
    cname = ""
    while line:
      hasName = re.search("^FN:(.*)$", line)
      if hasName and len(hasName.groups()) >= 1:
        cname = hasName[1]
        if checkName(name, cname):
          found = True
      if found:
        hasNumber = re.search("^TEL;.*:(.*)$", line)
        if hasNumber and len(hasNumber.groups()) >= 1:
          return (cname, hasNumber[1])
      line = contacts.readline()

  return None

def incoming_matrix_invite(matrix_thread_api, room):
  global matrix_api

  with lock_log:
    logger.debug("[incoming_matrix_invite] - called")
    logger.info("[incoming_matrix_invite] - invite for room %s", room)

  db = sqlite3.connect(database_file)

  matrix_join_room(matrix_thread_api, room)
  topic = matrix_get_topic(matrix_thread_api, room)
  topic = topic.replace(" ", "")
  with lock_log:
    logger.debug("[incoming_matrix_invite] - topic: -%s-", topic)

  try:
    numbers = topic.split(",")
  except Exception as problem:
    logger.debug('[incoming_matrix_invite] - cant make list from topic: %s',problem)
    matrix_send_text_emote(matrix_thread_api, room, "problem in topic: "+numbers)
    matrix_leave_room(matrix_thread_api, room)
    db.close
    return None

  bad_number=False

  formatted_numbers=[formatted_cell_number]
  for number in numbers:
    if ( test_number(number) ):
      formatted_numbers.append(format_number(number))
    else:
      contact = find_contact(number) 
      with lock_log:
        logger.debug("[incoming_matrix_invite] results from find_contact: %s" %(str(contact)))
      if contact:
        (cname, cnumber) = contact
        cnumber = format_number(str(cnumber))
        formatted_numbers.append(cnumber)
        matrix_send_text_emote(matrix_thread_api, room, f"found contact {cname} with number {cnumber}") 
        try: matrix_api.set_room_topic(room, cnumber)
        except Exception as problem:
          with lock_log:
            logger.error('[matrix_build_room] - failed to set topic to new room %s',problem)
          matrix_send_text_emote(matrix_thread_api, room, "Failed to update room topic with number: %s" % (cnumber)) 
          matrix_send_text_emote(matrix_thread_api, room, "Make sure the change topic manually before using this room") 
        matrix_send_text_emote(matrix_thread_api, room, "Updated room topic with number: %s" % (cnumber)) 

      else:
        bad_number=True
        with lock_log:
          logger.debug("[incoming_matrix_invite] problem with: %s", number)
        matrix_send_text_emote(matrix_thread_api, room, "problem with topic: "+number)
        matrix_send_text_emote(matrix_thread_api, room, "not proper number and cannot find contact for it")
        
  if(bad_number):
    matrix_leave_room(matrix_thread_api, room)
    db.close
    return None

  #look for a room.
  room_id = database_get_room(db, formatted_numbers)


  #if no room, we're good. Make it
  if (room_id == 0 ):
    with lock_log:
      logger.info("[matrix_deal_with_invites] - building new local room.")

    rooms_id = database_build_room(db, formatted_numbers)

    database_add_matrix_room(db,rooms_id,room)
    matrix_send_text_emote(matrix_thread_api, room, "room added to db: "+str(rooms_id))

  #we already have a room for this person. Leave matrix room.
  else:
    with lock_log:
      logger.debug("[matrix_deal_with_invites] - already have a room for these people.")
    matrix_send_text_emote(matrix_thread_api, room, "already have a room for this: "+str(room_id))
    matrix_leave_room(matrix_thread_api, room)
    db.close
    return None



  db.close
  return 0


#sending messages
def send_sms_message(phone_number, message ):
  with lock_log:
    logger.debug("[send_sms_message] - Called.")

  account = get_purple_account()
  if not account:
    return None
     
  bus = dbus.SessionBus()
  mm = dbus.Interface(bus.get_object(purpleDbus, purpleObj),
                              purpleDbus+'.PurpleConversation')

  #mm.SetProperty("UseDeliveryReports", dbus.Boolean(1) )

  conv = mm.PurpleConversationNew(1, account, phone_number)
  convIm = mm.PurpleConvIm(conv)
   

  try:
    mm.PurpleConvImSend(convIm, message)
    with lock_log:
      logger.info("[send_sms_message] - Sent an SMS")
      logger.debug("[send_sms_message] - SMS (%s) - %s",phone_number, message)
    return "%s: %s" % (phone_number, message)
  except Exception as problem: 
    with lock_log:
      logger.critical("[send_sms_message] - Error Sending SMS: %s: %s" % (phone_number, message))
    return None

def send_mms_message(phone_numbers, text, file_url):
  with lock_log:
    logger.debug("[send_mms_message] - called.")


  if type(phone_numbers) is not list:
    with lock_log:
      logger.critical("[send_mms_message] - Phone numbers needs to be a list, got", type(phone_numbers))

  bus = dbus.SessionBus()
  services = bus.call_blocking("org.ofono.mms", "/org/ofono/mms",
                               "org.ofono.mms.Manager",
                               "GetServices",
                               None, [], timeout=5)
  path = services[0][0]


  outbound_dir=attachments_path+"/outbound/"
  Path(outbound_dir).mkdir(parents=True, exist_ok=True)
  outbound_mms = outbound_dir+str(int(time.time()))+"-"+str(random.randrange(10, 99))+formatted_cell_number+".mms"

  mms_attachment_info =[]
  if ( text != None ):
    with lock_log:
      logger.debug("[send_mms_message] - text to send.")
    #Try to write text to oubound mms file
    try:
      out_file = open(outbound_mms, "w")
      out_file.write(text)
    except Exception as problem:
      with lock_log:
        logger.critical("[send_mms_message] - Failed to write MMS text file: %s", problem)
      return None

    out_file.close()

    mms_attachment_info.append("text.txt,text/plain,"+outbound_mms)


  if (file_url != None):
    with lock_log:
      logger.debug("[send_mms_message] - download and send file")

    #Try to download file to outbound_mms location.
    try:
      urllib.request.urlretrieve(file_url, outbound_mms)
    except Exception as problem:
      with lock_log:
        logger.critical("[send_mms_message] - Failed to download matrix file. %s", problem)
      return None

    mms_attachment_info.append("pic.jpg,image/jpeg,"+outbound_mms)


  attachments = dbus.Array([],signature=dbus.Signature('(sss)'))
  for a in mms_attachment_info:
    with lock_log:
      logger.debug("Attachment: (%s)" % a)
    reader = csv.reader([a])
    for r in reader:
      attachments.append(dbus.Struct((dbus.String(r[0]),
                                      dbus.String(r[1]),
                                      dbus.String(r[2])
                                     ), signature=None))

  try:
    path = bus.call_blocking("org.ofono.mms", path,
                             "org.ofono.mms.Service",
                             'SendMessage',
                              None, (phone_numbers, attachments), timeout=5)
    with lock_log:
      logger.info("[send_mms_message] - sent MMS")
      logger.debug("[send_mms_message] - MMS (%s) %s", phone_numbers, path)
  except Exception as problem: 
    with lock_log:
      logger.critical("[send_mms_message] - Failed to send. %s", problem)
    path = "error"
 


  with lock_log:
    logger.debug("[send_mms_message] - Deleteing outbound dir. TODO-only delete file on success.")
  try:
    shutil.rmtree(outbound_dir)
    with lock_log:
      logger.info("[send_mms_message] - deleted attachments: %s", outbound_dir)
  except OSError as e:
    with lock_log:
      logger.error("[send_mms_message] - failed : %s" % (e.strerror))


 
  return path

def matrix_send_text(matrix_room, message):
  global matrix_api
  with lock_log:
    logger.debug("[matrix_send_text] - called.")

  #if its a group MMS add the sender to the message.
  #TODO - Contacts? This is where we'd add it for MMSes.
  if ( len(message["recipients"]) > 1 ):
    message["content"] = message["sender"]+": "+message["content"] 


  content_pack = {
    "msgtype": "m.text",
    "body": message["content"],
    "db_message_id": message["id"],
    "db_rooms_id": message["rooms_id"],
    "sender": message["sender"],
    "recipients": message["recipients"],
    "attempt": message["attempt"],
    "localsenttime": message["localsenttime"],
    "ofono_type": message["ofono_type"],
    "content_type": message["content_type"],
  }


  try:
    matrix_id = matrix_api.send_message_event(room_id=matrix_room,event_type="m.room.message",content=content_pack)
    with lock_log:
      logger.info("[matrix_send_text] - bridged SMS to matrix.")
      #logger.debug("[matrix_send_text] - SMS to matrix room $s (%s) - %s.", matrix_room, message["sender"], message["content"])      
  except Exception as problem:
    with lock_log:
      logger.error("[matrix_send_text] - error trying to send message to matrix: %s", problem)
    return None

  #This checks matrix_id from above for an event_id from matrix.
  #if it has one the message was sent successfuly. 
  if(matrix_id['event_id']):
    with lock_log:
      logger.debug("[matrix_send_text] - matrix return id: %s", matrix_id['event_id'])
    return matrix_id['event_id']
  else:
    with lock_log:
      logger.error("[matrix_send_text] - Something wrong with what matrix returned: %s",matrix_id )
    return None

def matrix_send_file(matrix_room, message):
  global matrix_api
  with lock_log:
    logger.debug("[matrix_send_file] - called")


  #read in bites to send to matrix.
  mms_file = open(message["content"], "rb")
  mms_message_attachment  = mms_file.read() 
  mms_file.close()

  #upload file and get mxc link.
  try: 
    matrix_mxc_url=matrix_api.media_upload(content_type=message["content_type"], content=mms_message_attachment)
  except Exception as problem:
    logger.error('[matrix_send_file] - Failed trying to upload file %s', problem)
    return None

  if(matrix_mxc_url['content_uri']):
    with lock_log:
      logger.debug("[matrix_send_file] - matrix file upload link: %s", matrix_mxc_url['content_uri'])
  else:
    with lock_log:
      logger.error("[matrix_send_file] - Matrix didn't respond correctly to our file upload: %s", matrix_mxc_url )
    return None

  if ("image" in message["content_type"]):
    msgtype = "m.image"
  else:
    msgtype = "m.file"

  content_pack = {
    "msgtype": msgtype,
    "body": message["content"],
    "url": matrix_mxc_url['content_uri'],
    "db_message_id": message["id"],
    "db_rooms_id": message["rooms_id"],
    "sender": message["sender"],
    "recipients": message["recipients"],
    "attempt": message["attempt"],
    "localsenttime": message["localsenttime"],
    "ofono_type": message["ofono_type"],
    "content_type": message["content_type"],
  }

  #send the mxc link to matrix as a m.room.message with a type of m.image.
  try:
    matrix_id = matrix_api.send_message_event(room_id=matrix_room,event_type="m.room.message",content=content_pack)
    with lock_log:
      logger.info("[matrix_send_file] - bridged MMS to matrix.")
      #logger.debug("[matrix_send_file] - MMS to matrix room $s (%s) - %s.", matrix_room, message["sender"], message["content"])      

  except Exception as problem:
    with lock_log:
      logger.error("[matrix_send_file] - Error sending message with our mxc url to matrix: %s", problem)
    return None

  if(matrix_id['event_id']):
    with lock_log:
      logger.debug("[matrix_send_file] - matrix return id: %s", matrix_id['event_id'])
    return matrix_id['event_id']
  else:
    with lock_log:
      logger.error("[matrix_send_file] - Matrix didn't respond as expected to our message with mxc url: %s",matrix_id )
    return None


#the main Bridge code for both directions
def bridge_ofono_to_matrix(ofono_message_pack):
  with lock_log:
    logger.debug("[bridge_ofono_to_matrix] - called.")
    logger.debug("[bridge_ofono_to_matrix] - pack: %s ",ofono_message_pack)

  db = sqlite3.connect(database_file)

  if(ofono_message_pack["attempt"] == 0):
    ofono_message_pack["attempt"] = 1


    #look for a room
    room_members = ofono_message_pack["recipients"].copy()
    room_members.append(ofono_message_pack["sender"])
    ofono_message_pack["rooms_id"] = database_get_room(db, room_members)

    
    
    #if there's no room, make one.
    if( ofono_message_pack["rooms_id"] == 0):
      ofono_message_pack["rooms_id"] = database_build_room(db, room_members)

    room_members.clear()


    if( ofono_message_pack["ofono_type"] == Ofono_types.MMS.value):
      #MMSes have an attachment that needs to be dealt with no matter what
      attachment_info = deal_with_mms_attachment(db, ofono_message_pack["rooms_id"], ofono_message_pack["sender"], ofono_message_pack["content"] )
      ofono_message_pack["content_type"] = attachment_info[0]
      ofono_message_pack["content"] = attachment_info[1]

    #write to database and get return ID.
    ofono_message_pack["id"] = database_write_message(db, ofono_message_pack)


  #if this is not the first attempt-
  else:
    ofono_message_pack["attempt"] = ofono_message_pack["attempt"] + 1
    with lock_log:
      logger.warning("[bridge_ofono_to_matrix] - attempt %s for %s.",ofono_message_pack["attempt"],ofono_message_pack["id"])



  #check for a room in matrix, if none build one.
  matrix_room = database_check_for_matrix_room(db, ofono_message_pack["rooms_id"])
  if( not matrix_room ):
    matrix_room = matrix_build_room(db, ofono_message_pack)




  if(matrix_room != None):
    if ("text/plain" in ofono_message_pack["content_type"]):
      event = matrix_send_text(matrix_room, ofono_message_pack)
    else:
      event = matrix_send_file(matrix_room, ofono_message_pack)

  else:
    event = "cant find room."

  ##check return code and update database.
  if(event != None and event[0] == "$"):
    with lock_log:
      logger.info("[bridge_ofono_to_matrix] - message successfully bridge to matrix.")
    database_mark_bridged_status(db, ofono_message_pack["id"], Message_status.BRIDGED.value, ofono_message_pack["attempt"], event)
  else:
    with lock_log:
      logger.warning("[bridge_ofono_to_matrix] - failed to bridge to matrix.")
    database_mark_bridged_status(db, ofono_message_pack["id"], Message_status.ERROR.value, ofono_message_pack["attempt"], "")

  db.close
  return 0

def bridge_matrix_to_ofono(matrix_thread_api, matrix_message_pack):
  with lock_log:
    logger.debug("[bridge_matrix_to_ofono] - called.")
    logger.debug("[bridge_matrix_to_ofono] -Message pack to bridge: %s ",matrix_message_pack)

  db = sqlite3.connect(database_file)


  if(matrix_message_pack["attempt"] == 0):
    matrix_message_pack["attempt"] = 1


    rooms_info = database_matrix_room_to_rooms(db, matrix_message_pack["matrix_room"])

    #If there is nothing in the DB, let the user know and leave the room.
    if (rooms_info == None):
      with lock_log:
        logger.warning("[bridge_matrix_to_ofono] - Bot is in a room but its not in the DB. Leaving matrix room.")
      matrix_send_text_emote(matrix_thread_api, matrix_message_pack["matrix_room"], "This room isn't in the database. Make a new room and invite me.")
      matrix_leave_room(matrix_thread_api, matrix_message_pack["matrix_room"])
      db.close
      return None

    matrix_message_pack["rooms_id"] = rooms_info[0]
    #remove the ID we just used so we keep the list of one element.
    del rooms_info[0]

    if ( rooms_info[0].startswith( 'group' ) ):
      ##deal with group message
      with lock_log:
        logger.debug("[bridge_matrix_to_ofono] - Group message. Looking for memebers.")
      matrix_message_pack["recipients"] = database_get_list_of_group_members(db, rooms_info[0])
    else:
      matrix_message_pack["recipients"] = rooms_info


    if(matrix_message_pack["content_type"] == "m.text" and len(matrix_message_pack["recipients"]) == 1 ):
      matrix_message_pack["ofono_type"] = Ofono_types.SMS.value
    else:
      matrix_message_pack["ofono_type"] = Ofono_types.MMS.value

    #write to database and get return ID.
    matrix_message_pack["id"] = database_write_message(db, matrix_message_pack)

    #Mark it as read to show its safely in the db.
    matrix_read_receipt(matrix_thread_api, matrix_message_pack["matrix_room"], matrix_message_pack['event'])



  else:
    matrix_message_pack["attempt"] = matrix_message_pack["attempt"] + 1
    

  #get file from mxc if we need to
  matrix_file_url=None
  if( matrix_message_pack["content_type"] != "m.text" ):
    try:
      matrix_file_url=matrix_thread_api.get_download_url(matrix_message_pack["content"])
    except Exception as problem: 
      with lock_log:
        logger.error("[bridge_matrix_to_ofono] - failed to get file url. %s", problem)



  #If it's m.text from matrix send an SMS unless its a group chat.
  if( matrix_message_pack["content_type"] == "m.text" ):
    if(len(matrix_message_pack["recipients"]) == 1):
      event = send_sms_message(matrix_message_pack["recipients"][0],matrix_message_pack["content"])

   #If its not m.image or m.text just send a link. Group chat needs MMS.
  #Other files (pdf,wav,mp3) can be sent over MMS too and that can be
  #set up here. You can implement it if you'd like it :)
  else:
    if(len(matrix_message_pack["recipients"]) == 1):
      event = send_sms_message(matrix_message_pack["recipients"][0],matrix_file_url)

  ##if we get a path (starts with /) then it was passed to ofono for sending.
  #the ofono signal_receiver will update the message as brdiged.
  if( event ):
    with lock_log:
      logger.info("[bridge_matrix_to_ofono] - Matrix message sent to ofono.")
    database_mark_bridged_status(db, matrix_message_pack["id"], Message_status.TRYING.value, matrix_message_pack["attempt"], event)

  else:
    with lock_log:
      logger.warning("[bridge_matrix_to_ofono] - failed to send message")
    #TODO - Notify the user.
    database_mark_bridged_status(db, matrix_message_pack["id"], Message_status.ERROR.value, matrix_message_pack["attempt"], "")


    
  db.close
  

#the four threads that start all the work.
def ofono_sms():
  with lock_log:
    logger.info('[ofono_sms] starting SMS Listener....')

  #There's probably a better way to do this
  dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
  bus = dbus.SessionBus()
  bus.add_signal_receiver(incoming_sms_message,
                          bus_name=purpleDbus,
                          signal_name = "ReceivedImMsg")

  #ofono confirm SMS was sent.
  bus.add_signal_receiver(outgoing_sms_message_added,
                          bus_name=purpleDbus,
                          signal_name = "SendingImMsg")

  bus.add_signal_receiver(outgoing_sms_message_removed,
                          bus_name=purpleDbus,
                          signal_name = "SentImMsg")
                          
  mainloop = GLib.MainLoop()
  mainloop.run()

def incoming_sms_message(account, sender, message, conv, flags):
  if account != get_purple_account():
    return
    
  with lock_log:
    logger.debug("[incoming_sms_message] - Called.")
    logger.info('[incoming_sms_message] - Incoming SMS')

  new_sms = {
    "id": 0,
    "rooms_id": 0,
    "sender": "",
    "sender_name": "",
    "recipients": "",
    "direction": 0,
    "localsenttime": "",
    "ofono_type": 0,
    "content_type": "",
    "content": "",
    "file_size": 0,
    "attempt": 0,
    "smil": ""
  }
  
  new_sms["direction"] = Ofono_direction.IN.value
  new_sms["ofono_type"] = Ofono_types.SMS.value
  new_sms["content_type"] = "text/plain;charset=utf-8"
  new_sms["attempt"] = 0
  new_sms["recipients"] = [formatted_cell_number]

  
  new_sms["content"] = message
  new_sms["sender"] = format_number(sender)
  new_sms["localsenttime"] = str(datetime.datetime.now())

  bus = dbus.SessionBus()
  #conversation manager
  cm = dbus.Interface(bus.get_object(purpleDbus, purpleObj),
                                    purpleDbus+'.PurpleConversation')
  # TODO add to db for possible second attempt
  new_sms["sender_name"] = cm.PurpleConversationGetTitle(conv)
      
  bridge_ofono_to_matrix(new_sms)

def outgoing_sms_message_added(account, receiver, message):
  with lock_log:
    logger.debug("[ofono_message_added] - Called.")
    logger.debug("[ofono_message_added] - SMS queue: %s to %s" % (message, receiver))

def outgoing_sms_message_removed(account, receiver, message):
  content = "%s: %s" % (receiver, message)

  with lock_log:
    logger.debug("[outgoing_sms_message_removed] - Called.")
    logger.debug("[outgoing_sms_message_removed] - SMS sent: %s" % (content))


  #This can be called before the script updates the database meaning the
  #status will forever be suck in pending even though it should be marked
  #as something else. We'll try a couple times then give up.
  time.sleep(1)
  for x in range(3):
    db = sqlite3.connect(database_file)
    db_updated = database_ofono_sent_status(db, content, Message_status.BRIDGED.value)
    db.close
    if(db_updated == 0):
        break
    time.sleep(1)


  return 0
  

def ofono_mms():
  with lock_log:
    logger.info('[ofono_mms] starting MMS Listener....')

  #https://kernel.googlesource.com/pub/scm/network/ofono/mmsd/+/master/test/monitor-mms

  dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
  bus = dbus.SessionBus()
  bus.add_signal_receiver(incoming_mms_message,
                          bus_name="org.ofono.mms",
                          signal_name = "MessageAdded",
                            member_keyword="member",
                            path_keyword="path",
                            interface_keyword="interface")

  bus.add_signal_receiver(mms_property_changed,
                          bus_name="org.ofono.mms",
                          signal_name = "PropertyChanged",
                            member_keyword="member",
                            path_keyword="path",
                            interface_keyword="interface")



  mainloop = GLib.MainLoop()
  mainloop.run()

def incoming_mms_message(name, value, member, path, interface):
  with lock_log:
    logger.debug("[incoming_mms_message] - Called.")
    logger.info("[incoming_mms_message] - Incoming MMS")

  new_mms = {
    "id": 0,
    "rooms_id": 0,
    "sender": "",
    "recipients": "",
    "direction": 0,
    "localsenttime": "",
    "ofono_type": 0,
    "content_type": "",
    "content": "",
    "file_size": 0,
    "attempt": 0,
    "smil": ""
  }


  new_mms["direction"] = Ofono_direction.IN.value
  new_mms["ofono_type"] = Ofono_types.MMS.value



  for mms_object in value:
    if (mms_object == "Sender"):
      new_mms["sender"] = format_number(value[mms_object])


    elif (mms_object == "Date"):
      new_mms["localsenttime"] = value[mms_object]

      
    elif (mms_object == "Smil"):
    #I dont know what we'd use this for
      with lock_log:
        logger.debug("[incoming_mms_message] - I dont use Smil: %s", value[mms_object])
      continue
      
      
    elif (mms_object == "Status"):
      #I dont know what we'd use this for
      with lock_log:
        logger.debug("[incoming_mms_message] - I dont use Status: %s", value[mms_object])
      continue

    elif (mms_object == "Attachments"):
      mms_attachments = value[mms_object]
      
    elif (mms_object == "Recipients"):
    #Take each recipent and format, then store it.
      new_recipients = []
      for recipient in value[mms_object]:
        new_recipients.append(format_number(recipient))
      new_mms["recipients"] = new_recipients

    else:
      with lock_log:
        logger.debug("[incoming_mms_message] - %s has not been implemented", mms_object)


  #When sending an MMS this is called but everything is empty.
  if(new_mms["sender"] == "" or new_mms["sender"] in formatted_cell_number ):
    with lock_log:
      logger.info("[incoming_mms_message] - We are sending an mms... exiting. ")
    return None

      
  #TODO - if there are no attachments, there's an error  

  for attachment in mms_attachments:
    with lock_log:
      logger.debug("[incoming_mms_message] - Found an MMS Attachment.")
    new_mms["attempt"] = 0
    new_mms["content"] = attachment
    bridge_ofono_to_matrix(new_mms)


  return 0

def mms_property_changed(name, value, member, path, interface):
  with lock_log:
    logger.debug("[mms_property_changed] - Called.")

  #This can be called before the script updates the database meaning the
  #status will forever be suck in pending even though it should be marked
  #as something else. We'll try a couple times then give up.
  time.sleep(1)
  for x in range(3):

    db = sqlite3.connect(database_file)

    if(value == "Sent"):
      with lock_log:
        logger.debug("[mms_property_changed] - MMS sent: %s" % (path))
      db_updated = database_ofono_sent_status(db, path, Message_status.BRIDGED.value)

    
    elif(value == "TransientError"):
      with lock_log:
        logger.critical("[mms_property_changed] MMS TransientError - Problem with cell data, need to retry - %s" % (path))
      db_updated = database_ofono_sent_status(db, path, Message_status.ERROR.value)


    else:
      #TODO - Deal with other status??
      with lock_log:
        logger.debug("[mms_property_changed] - value %s not implemented. path - %s" % (value, path))
      break

    db.close

    if(db_updated == 0):
      break
    time.sleep(1)
      
  return 0

def retry_failed_task( failed_messages_matrix_api ):
  with lock_log:
    logger.debug("[retry_failed_task] - Called.")
    logger.info("[retry_failed_thread] - Thread started")

  time.sleep(check_failed_messages_every_seconds)

  while True:
    #TODO- I dont know how else to do this
    global matrix_api

    #TODO- ofono tries to retry sending SMSes (airplane mode). I not
    #sure it lets us know when it gives up but it does eventually.
    #messages could be stuck in pending(2) status.
    
    db = sqlite3.connect(database_file)
    cursor = db.cursor()
    #Check for failed ofono -> matrix messages.
    cursor.execute('''SELECT id,rooms_id,sender,direction,time,ofono_type,content_type,content,file_size,bridged_attempt FROM messages WHERE bridged_status>=? AND bridged_attempt <=?''', (Message_status.ERROR.value , max_number_of_retries,))
    messages_row = cursor.fetchone()
    db.commit()

    #if there is a failed ofono -> matrix message try to build it.
    if ( messages_row != None ):
      with lock_log:
        logger.warning("[retry_failed_thread] - Failed Message ID %s",messages_row[0])

      cursor.execute('''select member from rooms where id=?''', (messages_row[1], ))
      rooms_row = cursor.fetchone()
      db.commit()
      db.close

      if ( rooms_row != None ):
        with lock_log:
          logger.debug("[retry_failed_thread] - rooms_member %s",rooms_row[0])

        if(rooms_row[0].startswith('group')):
          recipients = database_get_list_of_group_members(db, rooms_row[0])
        else:
          recipients = [rooms_row[0]]

        new_failed_message = {
          "id": messages_row[0],
          "rooms_id": messages_row[1],
          "sender": messages_row[2],
          "recipients": recipients,
          "direction": messages_row[3],
          "localsenttime": messages_row[4],
          "ofono_type": messages_row[5],
          "content_type": messages_row[6],
          "content": messages_row[7],
          "file_size": messages_row[8],
          "attempt": messages_row[9],
          "smil": ""
        }

        if(new_failed_message["direction"] == Ofono_direction.IN.value):
          bridge_ofono_to_matrix(new_failed_message)
        elif(new_failed_message["direction"] == Ofono_direction.OUT.value):
          bridge_matrix_to_ofono(matrix_api, new_failed_message)


      else:
        #TODO - do something here. We could get stuck in a loop.
        #Maybe +1 the retry above?
        with lock_log:
          logger.critical("[retry_failed_thread] - No room in rooms table.")

    else:
      db.close
      with lock_log:
        logger.info("[retry_failed_thread] - No failed messages.")



    time.sleep(check_failed_messages_every_seconds)

  return None




def incoming_matrix_message(matrix_thread_api, room_id, event):
  with lock_log:
    logger.debug("[incoming_matrix_message] - called.")
    
  new_matrix_message = {
    "id": 0,
    "rooms_id": 0,
    "sender": "",
    "recipients": "",
    "direction": 0,
    "localsenttime": "",
    "ofono_type": 0,
    "content_type": "",
    "content": "",
    "attempt": 0,
    "matrix_room": "",
    "file_name": "",
    "file_size": 0,
    "event": "",
    "smil": ""
  }
  
  #if the event is redacted, trying to get the event['content']['msgtype'] will crash.
  new_matrix_message["content_type"] = None
  try:
    new_matrix_message["content_type"] = event['content']['msgtype']        
  except:
    with lock_log:
      logger.debug("[incoming_matrix_message] - Event is redacted")

  if(new_matrix_message["content_type"] != None):
    with lock_log:
      logger.debug("[incoming_matrix_message] - Event is a %s ", new_matrix_message["content_type"])

    new_matrix_message["matrix_room"] = room_id
    new_matrix_message["localsenttime"] = event['origin_server_ts']
    new_matrix_message["event"] = event['event_id']
    new_matrix_message["direction"] = Ofono_direction.OUT.value 
    new_matrix_message["sender"] = formatted_cell_number 

    if( new_matrix_message["content_type"] == "m.text" ):
      with lock_log:
        logger.info("[incoming_matrix_message] - Incoming Matrix text.")
      new_matrix_message["content"] = event['content']['body']

    else:
      with lock_log:
        logger.info("[incoming_matrix_message] - Incoming Matrix file.")
      new_matrix_message["file_name"] = event['content']['body']
      new_matrix_message["content"] = event['content']['url']
      new_matrix_message["ofono_type"] = Ofono_types.MMS.value
      
      #Some clients (libpurple) do not post the file size like riot does.
      try: new_matrix_message["file_size"] = event['content']['info']['size']
      except:
        with lock_log:
          logger.warning("[incoming_matrix_message] - User client does not post file size like riot. Assuming its too big.")
        new_matrix_message["file_size"] = mms_size_limit + 1

    bridge_matrix_to_ofono(matrix_thread_api, new_matrix_message)

def matrix_sync_task(matrix_thread_api, sync_since):

  with lock_log:
    logger.debug("[matrix_sync_task] - called.")
    logger.info("[matrix_sync_task] - Thread started")


  while True:
    with lock_log:
      logger.debug("[matrix_sync_task] - Sync with Matrix since %s",sync_since)

    #Try to sync, if it fails keep the old sync_time and try again.
    previous_sync = sync_since
    try:
      with lock_log:
        logger.info("[matrix_sync_task] - waiting for matrix event...")
      #sync to server
      sync_info = matrix_thread_api.sync(since=sync_since,set_presence="online",timeout_ms=matrix_sync_timeout_ms)
      #set new sync time
      sync_since = sync_info["next_batch"]
    except Exception as problem:
      sync_since = previous_sync
      with lock_log:
        logger.critical("[matrix_sync_task] - Failed to sync. %s", problem)
      time.sleep(matrix_sync_every_seconds)
      continue


    #update the sync time in the DB.
    db = sqlite3.connect(database_file)
    database_matrix_update_sync(db, sync_since)
    db.close


    #go though all the new events for each room.
    for room_id, syncRoom in sync_info['rooms']['join'].items():
      for event in syncRoom['timeline']['events']:


        #if the event is a m.room message, bridge it.
        if event['sender'] == matrix_user and event['type'] == 'm.room.message':
          with lock_log:
            logger.info("[matrix_sync_task] - new message to bridge.")
          incoming_matrix_message(matrix_thread_api, room_id, event)


        #if the user changed the name of the room, change our display name to match.
        elif event['sender'] == matrix_user and event['type'] == 'm.room.name':


          new_name = None
          try:
            new_name = event['content']['name'];
            with lock_log:
              logger.info("[matrix_sync_task] - Matrix room name change. ")
            
          except Exception as problem:
            with lock_log:
              logger.warning("[matrix_sync_task] - Matrix room name change likely redacted.")
            continue;

          if(new_name != None):
            with lock_log:
              logger.debug("[matrix_sync_task] - room name changed to: %s Changing name.", new_name )
            matrix_set_display_name( matrix_thread_api, room_id, new_name )


        #if the user leaves the room, clean up everything and also leave the room.
        elif event['sender'] == matrix_user and event['type'] == 'm.room.member' and event['content']['membership'] == 'leave':
          with lock_log:
            logger.info("[matrix_sync_task] - Matrix user left a room. Cleaning up local files.")

          db = sqlite3.connect(database_file)
          rooms_id = database_delete_everything_for_room(db, room_id)
          db.close

          #clean up attachements for room.
          os_delete_attachments(rooms_id)

          try: matrix_leave_room(matrix_thread_api, room_id)
          except Exception as problem:
            with lock_log:
              logger.critical('[matrix_sync_task] - cant leave room. we are stuck in an unsused room: %s',problem)



    #check for invites
    invites = []
    for types, content in sync_info['rooms'].items():
      if types == "invite":
        for room in content:
          sender = content[room]['invite_state']['events'][0]['sender']
          if sender == matrix_user:
            with lock_log:
              logger.info("[matrix_sync_task] - New Room invite")
            incoming_matrix_invite(matrix_thread_api, room)


    #Done with the Sync info. Wait X seconds so we dont spam the server.
    time.sleep(matrix_sync_every_seconds)

  #outside of while true
  return None

def main():
  #I dont know how to get matrix_api into the sms bridge
  global matrix_api
  
  with lock_log:
    logger.debug("[main] - called.")
    logger.info("[main] starting...")
    

  #build up the db
  db = sqlite3.connect(database_file)
  build_database(db)

  account_info = database_check_matrix_account(db)
  if ( account_info != None and account_info[0] != "" and account_info[1] != ""):
    with lock_log:
      logger.info("[main] - already logged in.")
    token = account_info[0]
    first_sync_time  = account_info[1]

    matrix_api = MatrixHttpApi(matrix_url, token=token)

    with lock_log:
      logger.debug("[main] - token "+token )
    with lock_log:
      logger.debug("[main] - sync "+first_sync_time )


  else:
    with lock_log:
      logger.info("[Main] trying to login to Matrix")

    #Login to Matrix via MatrixClient
    client = MatrixClient(matrix_url)
    try:
      token = client.login(username=bot_user, password=bot_pass)
      with lock_log:
        logger.debug('[main] - logged in. Token=%s',token)
      #From here we use MatrixHttpApi as matrix_api
      matrix_api = MatrixHttpApi(matrix_url, token=token)
    except:
      with lock_log:
        logger.critical('[main] - Matrix login exception')
      exit

    with lock_log:
      logger.info("[main] - Everything in this initial sync is ignored.")

    #Get the current sync_time and start bridging messages after now
    #this takes forever on old accounts - find a better way to do this?
    first_sync = matrix_api.sync(set_presence="online")
    first_sync_time = first_sync["next_batch"]
    with lock_log:
      logger.debug('[main] - starting threads at Sync time: %s',first_sync_time)


    database_matrix_update_token(db, token)
    database_matrix_update_sync(db, first_sync_time)


  db.close 

  

  thread_list = []

  ofono_sms_thread = threading.Thread(target=ofono_sms, args=())
  ofono_sms_thread.start()
  thread_list.append(ofono_sms_thread)

  """ NO MMS SUPPORT
  ofono_mms_thread = threading.Thread(target=ofono_mms, args=())
  ofono_mms_thread.start()
  thread_list.append(ofono_mms_thread)
  """

  matrix_thread = threading.Thread(target=matrix_sync_task, args=(matrix_api,first_sync_time))
  matrix_thread.start()
  thread_list.append(matrix_thread)

  if( max_number_of_retries > 0 and check_failed_messages_every_seconds > 0):
    retry_failed_thread = threading.Thread(target=retry_failed_task, args=(matrix_api,))
    retry_failed_thread.start()
    thread_list.append(retry_failed_thread)



  with lock_log:
    logger.debug('[main] - thread_list: %s',thread_list)

if __name__ == "__main__":
  main()
